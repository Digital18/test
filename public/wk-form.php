<?php

if (!empty($_POST) ) {
    $first_name = $_POST['first_name'];
    $last_name = $_POST['last_name'];
    $company = $_POST['company'];
    $email = $_POST['email'];
    $privacy = $_POST['00N0Y00000Rwgtv'];
    $phone = $_POST['phone'];

    $oid = '00D1r000000g5op';
    $lead_source = 'web_demodownload';
    $country = $_POST['country'];
    $currency = $_POST['currency'];
    $member_status = "Responded";
    //$Campaign_ID = "7011r0000019uWq";
    $Campaign_ID = $_POST['Campaign_ID'];
    $file_url = $_POST['file_url'];


    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://webto.salesforce.com/servlet/servlet.WebToLead?encoding=UTF-8",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => "oid=". $oid . "&lead_source=web_demodownload&Campaign_ID=" . $Campaign_ID . "&country=" . $country . "&currency=" . $currency . "&member_status=" . $member_status . "&first_name=" . $first_name . "&last_name=" . $last_name ."&company=" . $company . "&email=" . $email . "&phone=" .$phone. "&00N0Y00000Rwgtv=1",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "content-type: application/x-www-form-urlencoded",
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        $data = array(
            'succes' => false,
            'errormsg' => $err

        );

        header('Content-Type: application/json');
        echo json_encode($data);
    } else {
        //$file_url = "http://en.unesco.org/inclusivepolicylab/sites/default/files/dummy-pdf_2.pdf";
        $data = array(
            'succes' => true,
            'downloadfile' => $file_url

        );
        header('Content-Type: application/json');

        echo json_encode($data);
    }

}
else {

    header("Location: /");



}

?>